(function($) {

/**
* Behavior for the moneyjs Drupal module.
 */
Drupal.behaviors.moneyjsmodule = {
  attach: function(context, settings) {
    var pricetoconvert = $('.field-type-commerce-price').first().text() || "$34.99";
    pricetoconvert = $.trim(pricetoconvert);
    var price = accounting.unformat(pricetoconvert);
    $('div#moneyjs > fieldset > legend').replaceWith("<legend>How much is " + price + " in your currency?</legend>");
    $('#price').html(price);
    if (typeof fx !== 'undefined' && fx.rates) {
      fx.rates = $.parseJSON(settings.moneyjsmodule.rates);
      fx.base = $.parseJSON(settings.moneyjsmodule.base);
    } else {
        var fxSetup = {
             rates : $.parseJSON(settings.moneyjsmodule.fx.rates),
             base : $.parseJSON(settings.moneyjsmodule.fx.base)
        }
    }
    var currencies = settings.moneyjsmodule.currencies;
    $('#currency').once('moneyjs', function() {

       $.each($.parseJSON(currencies), function(key, value) {
         $('#currency')
             .append($('<option>', {value: key }).text(value));
       });
    }),
    $('#currency').change(function() {
        var target = $(this).val();
        fx.settings = { from : fx.base , to : target };
        cval = fx.convert(price);
        var convertedValue = accounting.formatMoney(cval, {
           symbol: target,
           format: "%v %s"
        });
        $('#price').html(convertedValue.toString());
    });

  }
};

})(jQuery);
