This module provides support for the money.js currency conversion 
script from http://josscrowcroft.github.io/money.js/ along with support
for Open Exchange Rate currency conversion data 
at https://openexchangerates.org.

How to use

1. Install
2. Get API key from https://openexchangerates.org/ 
  (look for free developer key or just go to it at 
   https://openexchangerates.org/signup/free)
3. Enable module and look for its configuration in 
   admin/configuration/services/moneyjs
4. Download money.js from http://josscrowcroft.github.io/money.js/
5. Download accouting.js from http://josscrowcroft.github.io/accounting.js
4. If you have Libraries module, copy money.js and accounting.js 
   to sites/all/libraries/moneyjs/. 
   If not, just put them in the folder for this module.
5. Run cron to populate the latest conversion rates. 
   You can verify on the configuration page.

Now how to you get it to work on your site. Right now, that is done 
via a full HTML block: 
   Add something like the following to a custom block:
<div id="moneyjs">
    <fieldset style="margin:auto;width:225px">
       <legend>How much is $999.99 in your currency?</legend>
    <select name="currency" id="currency" style="width:200px;">
    </select>
    <h2 id="price" style="text-align:center">999.99</h2>
</fieldset> 
</div>

and add to a region from the the block configuration menu under structure.

Right now the legend in the HTML snippet above and in moneyjs.js is hard coded. 
This will change in the future once I abstract the JavaScript integration.

If you have Drupal commerce installed, the integration JavaScript will 
pull the price from the displayed product price and change the legend and 
the initial displayed price for the above HTML.

This has only been tested on one site, so your experience may be different.

Notes:
This module is not dependent upon any other module, but will use php curl,
the Libraries module and Drupal Commerce if available.

The free developer key only allows conversion from USD to any other currency.
