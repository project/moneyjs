<?php
/**
 * @file
 * The moneyjs admin include file.
 */

/**
 * Implements hook_form().
 *
 * Configuraton form, called by drupal_get_form().
 */
function moneyjs_form($form, &$form_state) {
  $form['moneyjs_cron'] = array(
    '#markup' => t('<p>Once you enter the App Id you can run the !cronlink to populate the exchange rates.</p>',
    array(
      '!cronlink' => l(t('cron job'), 'admin/config/services/moneyjs/cron'),)
    ),
  );
  $form['moneyjs_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Open exchange rate App Id'),
    '#default_value' => variable_get('moneyjs_api_key', ''),
    '#description' => t('Open exchange rate app id.  Obtain from !openxchgrate',
      array('!openxchgrate' => '<a href="https://openexchangerates.org">Open Exchange Rate</a>')),
    '#required' => TRUE,
  );
  $form['moneyjs_paid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open Exchange Rate subscriber.'),
    '#description' => t('Free users can only convert from USD. If you are a subscriber, checking this box will enable the ability to change the base currency for conversion.'),
    '#default_value' => variable_get('moneyjs_paid', FALSE),
  );

  $form['moneyjs_fx_rates'] = array(
    '#type' => 'textarea',
    '#title' => t('Exchange Rates Data'),
    '#default_value' => variable_get('moneyjs_fx_rates', ''),
    '#description' => t('Open exchange rates JSON data.  Populated from cron.'),
  );
  if (variable_get('moneyjs_paid')) {
    $form['moneyjs_fx_base'] = array(
      '#type' => 'select',
      '#title' => t('Exchange Base Data'),
      '#options' => _moneyjs_get_currencies_as_array(),
      '#default_value' => _moneyjs_get_default_base_currency(),
      '#description' => t('Open exchange rate base data.'),
    );
  }
  else {
    $form['moneyjs_fx_base'] = array(
      '#type' => 'textfield',
      '#title' => t('Exchange Base Data'),
      '#default_value' => variable_get('moneyjs_fx_base', 'USD'),
      '#description' => t('Open exchange rate base conversion currency unit.'),
      '#disabled' => TRUE,
    );
  }
  $form['moneyjs_currencies'] = array(
    '#type' => 'textarea',
    '#title' => t('Currencies'),
    '#default_value' => variable_get('moneyjs_currencies', ''),
    '#description' => t('Open exchange rate JSON currency name data.  Populated from cron.'),
  );
  // Shamelessly ripped from block.module. Who doesn't use this snippet of code.
  // Shamelessly ripped from js_inject.module.  Thanks bro.
  $php_access = (user_access('use PHP for settings') && module_exists('php'));

  $options = array(
    MONEYJS_PAGES_NOTLISTED => t('Add on every page except the listed pages.'),
    MONEYJS_PAGES_LISTED => t('Add on only the listed pages.'),
  );
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
  array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  if ($php_access) {
    $options[MONEYJS_PHP] = t('Add if the following PHP code outputs a nonzero value (PHP-mode, experts only).');
    $description .= ' ' . t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.',
    array('%php' => '<?php ?>'));
  }
  $form['moneyjs_visibility_type'] = array(
    '#type' => 'radios',
    '#title' => t('Add the money.js on specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('moneyjs_visibility_type', ''),
  );
  $form['moneyjs_visibility_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('moneyjs_visibility_pages', ''),
    '#description' => $description,
  );

  return system_settings_form($form);
}

/**
 * Get the base currency.
 */
function _moneyjs_get_default_base_currency() {
  return variable_get('moneyjs_fx_base', 'USD');
}

/**
 * Get a list the list of currencies as an array for a form select.
 */
function _moneyjs_get_currencies_as_array() {
  $currencies = variable_get('moneyjs_currencies', array('USD' => t('United States Dollar')));
  if (is_array($currencies)) {
    return $currencies;
  }
  $currency_array = array();
  foreach (json_decode($currencies) as $key => $value) {
    $currency_array[] = array($key => t($value));
  }
  return $currency_array;
}


/**
 * Run the moneyjs_cron() task from a link on the admin from.
 */
function _moneyjs_cron_run() {
  moneyjs_cron();
  drupal_set_message(t('Cron task ran succesfully.'));
  drupal_goto('admin/config/services/moneyjs');
}
